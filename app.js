// Declare express
const express = require('express');
const { render } = require('express/lib/response');

// Declare database and requitment
const JSONdb = require('simple-json-db');
const db = new JSONdb('database.json');
const db2 = new JSONdb('post.json');

// Declare port
const app = express();
const port = 3000;


// Set view engine
app.set('view engine', 'ejs');

//index(로그인)
app.get('/', (req, res) => {
    app.locals.title = db.get('title');
    app.locals.postUrl = db.get('posturl'); 
    app.locals.content = db.get('content');
    res.render('index');
})


//sign
app.get('/sign', (req, res) => {
    
    


    res.render('sign');
});

//edt
app.get('/edt/:edturl', (req, res) => {
    edtUrl = req.params.edturl;
    app.locals.edtUrl = db2.get('edturl');
    app.locals.movie = db2.get('movie');
    app.locals.article = db2.get('article');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.posterurl = db2.get('posterurl');
    dbIndex = db2.get('edturl').indexOf(edtUrl);

    if (dbIndex != -1) {
        app.locals.movie = db2.get('movie')[dbIndex];
        app.locals.article = db2.get('article')[dbIndex];
        app.locals.date = db2.get('date')[dbIndex];
        app.locals.posterurl = db2.get('posterurl')[dbIndex];
        res.render('edt');
    } else {
        res.send('Page not found :(')
    }
});

//view
app.get('/view', (req, res) => {
    app.locals.edtUrl = db2.get('edturl');
    app.locals.movie = db2.get('movie');
    app.locals.article = db2.get('article');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.posterurl = db2.get('posterurl');
    res.render('view');
});

//write
app.get('/write', (req, res) => {
    app.locals.postUrl = db.get('posturl');
    app.locals.name = db.get('name');
    app.locals.movie = db2.get('movie');
    app.locals.posterurl = db2.get('posterurl');

    //db.set('키(ex movie)', value);

    res.render('write');
});

//info
app.get('/info', (req, res) => {
    app.locals.postUrl = db.get('posturl');
    app.locals.title = db.get('title');
    app.locals.name = db.get('name');
    app.locals.content = db.get('content');
    app.locals.movie = db2.get('movie');
    res.render("info");
});

// Run app
app.listen(port, () => {
    console.log('App is live');
});